#!/bin/sh

set -x

trap 'kill -TERM $PID' TERM INT

if [ "$#" -eq 0 ]; then
  transmission-daemon -c /to_download -w /output -f -t -a *.*.*.* -p "$PORT" -g /config &
else
  transmission-daemon $@ &
fi

PID=$!
wait $PID
wait $PID
while [ -f /config/lock ]
do
  sleep 2
done

EXIT_STATUS=$?
